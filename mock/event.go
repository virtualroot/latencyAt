package mock

import "gitlab.com/latency.at/latencyAt"

type EventService struct {
	GetFn   func(key string) (*latencyAt.Event, error)
	AddFn   func(event *latencyAt.Event) error
	AddTxFn func(event *latencyAt.Event) (latencyAt.Tx, error)
}

func (s *EventService) Get(key string) (*latencyAt.Event, error) {
	return s.GetFn(key)
}

func (s *EventService) Add(event *latencyAt.Event) error {
	return s.AddFn(event)
}

func (s *EventService) AddTx(event *latencyAt.Event) (latencyAt.Tx, error) {
	return s.AddTxFn(event)
}

func (s *EventService) Purge() error {
	return nil
}

func (s *EventService) Healthy() error {
	return nil
}
