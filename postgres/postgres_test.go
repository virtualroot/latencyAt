package postgres

import (
	"log"
	"os"
	"runtime/debug"
	"testing"

	"github.com/jmoiron/sqlx"
)

/*
Create DB with:
---
cat <<EOF | sudo -u postgres psqlx -U postgres
CREATE ROLE latencyat_test LOGIN PASSWORD 'foobar23';
CREATE DATABASE latencyat_test OWNER latencyat_test;
GRANT ALL PRIVILEGES ON DATABASE latencyat_test TO latencyat_test;
EOF
cat <<EOF | sudo -u postgres psqlx -U postgres latencyat_test
CREATE EXTENSION IF NOT EXISTS citext;
EOF
---
*/

const (
	defaultDBURL = "postgres://latencyat_test:foobar23@localhost/latencyat_test"
)

func assert(t *testing.T, err error) {
	if err != nil {
		debug.PrintStack()
		t.Fatal(err)
	}
}

/*
type fatalPrintfer struct {
	*testing.T
}

func (p *fatalPrintfer) Printf(format string, a ...interface{}) {
	debug.PrintStack()
	p.Fatalf(format, a)
}*/

func initDB() (*sqlx.DB, error) {
	dbu := os.Getenv("DB_URL")
	if dbu == "" {
		dbu = defaultDBURL
	}
	db, err := sqlx.Open("postgres", dbu)
	if err != nil {
		return nil, err
	}

	if _, err := db.Exec(EmailTokenServiceSchema +
		UserServiceSchema +
		TokenServiceSchema); err != nil {
		return nil, err
	}
	log.Println("created db")
	return db, nil
}
