package postgres

import (
	"github.com/jmoiron/sqlx"
	"errors"

	"gitlab.com/latency.at/latencyAt"
)

const EmailTokenServiceSchema = `
CREATE TABLE IF NOT EXISTS email_token (
  id BIGSERIAL PRIMARY KEY,
  token varchar(64),
  created timestamp
);
`

// Ensure EmailTokenService implements latencyAt.EmailTokenService.
var _ latencyAt.EmailTokenService = &EmailTokenService{}

type EmailTokenService struct {
	db                *sqlx.DB
	stmtSelect        *sqlx.Stmt
	stmtSelectByToken *sqlx.Stmt
	stmtInsert        *sqlx.Stmt
	stmtDelete        *sqlx.Stmt
	stmtSetToken      *sqlx.Stmt
	stmtDeleteAll     *sqlx.Stmt
}

var ErrEmailTokenNotFound = errors.New("Email token not found")

func NewEmailTokenService(db *sqlx.DB) (*EmailTokenService, error) {
	es := &EmailTokenService{db: db}
	for ref, str := range map[**sqlx.Stmt]string{
		&es.stmtSelect:        "SELECT token, created from email_token where id = $1",
		&es.stmtSelectByToken: "SELECT id, token, created from email_token where token = $1",
		&es.stmtInsert:        "INSERT INTO email_token (token, created) VALUES ($1, $2) RETURNING id",
		&es.stmtDelete:        "DELETE from email_token where id = $1",
		&es.stmtSetToken:      "UPDATE email_token SET token = $2, created = $3 WHERE id = $1",
		&es.stmtDeleteAll:     "DELETE FROM email_token",
	} {
		s, err := db.Preparex(str)
		if err != nil {
			return nil, err
		}
		*ref = s
	}
	return es, nil
}

func (s *EmailTokenService) Healthy() error {
	return s.db.Ping()
}

func (s *EmailTokenService) EmailTokenByToken(token string) (*latencyAt.EmailToken, error) {
	rows, err := s.stmtSelectByToken.Query(token)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if !rows.Next() {
		return nil, ErrEmailTokenNotFound
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	t := &latencyAt.EmailToken{}
	return t, rows.Scan(&t.ID, &t.Token, &t.Created)
}

func (s *EmailTokenService) EmailToken(id int) (*latencyAt.EmailToken, error) {
	rows, err := s.stmtSelect.Query(id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if !rows.Next() {
		return nil, ErrEmailTokenNotFound
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	token := &latencyAt.EmailToken{ID: id}
	return token, rows.Scan(&token.Token, &token.Created)
}

// FIXME: I don't like how this is implicitly adding ID, is that how people are
// doing this?
func (s *EmailTokenService) CreateEmailToken(t *latencyAt.EmailToken) error {
	return s.stmtInsert.QueryRow(
		t.Token,
		t.Created,
	).Scan(&t.ID)
}

func (s *EmailTokenService) DeleteEmailToken(t *latencyAt.EmailToken) error {
	_, err := s.stmtDelete.Exec(t.ID)
	return err
}

func (s *EmailTokenService) UpdateEmailToken(t *latencyAt.EmailToken) error {
	_, err := s.stmtSetToken.Exec(t.ID, t.Token, t.Created)
	return err
}

func (s *EmailTokenService) Purge() error {
	_, err := s.stmtDeleteAll.Exec()
	return err
}
