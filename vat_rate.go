package latencyAt

type VatRate struct {
	Rate float64 `json:"rate"`
	InEU bool    `json:"inEU"`
}
