package main

import (
	"context"
	"log"
	"net/http"
	"strings"
	"time"

	"cloud.google.com/go/pubsub"

	lru "github.com/hashicorp/golang-lru"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/namsral/flag"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/errors"
	"gitlab.com/latency.at/latencyAt/postgres"
)

const cacheSize = 320000 // 64MB / ~20 byte

var (
	config = flag.String(flag.DefaultConfigFlagname, "", "Path to config file")

	databaseURL    = flag.String("db-url", "postgres://latencyat:foobar23@localhost/latencyat", "Database URL")
	databaseDriver = flag.String("db-driver", "postgres", "Database driver")
	projectID      = flag.String("pubsub-project-id", "latency-at", "Pub/Sub project id")
	topicName      = flag.String("pubsub-topic", "latency-exporter-requests", "Pub/Sub topic to subscribe to")
	subName        = flag.String("pubsub-sub", "lat-booker", "Subscription name")

	requestCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "lat",
			Subsystem: "booker",
			Name:      "requests_total",
			Help:      "Total number of processed requests in booker",
		},
	)
	messageCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "lat",
			Subsystem: "booker",
			Name:      "messages_total",
			Help:      "Total number of processed messages in booker",
		},
	)
	messageDupCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "lat",
			Subsystem: "booker",
			Name:      "messages_duplicated_total",
			Help:      "Total number of duplicated messages received by booker",
		},
	)
	errorCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "lat",
			Subsystem: "booker",
			Name:      "errors_total",
			Help:      "Total number of errors while processing messages in booker",
		},
		[]string{"severity"},
	)
	messageDurations = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Namespace: "lat",
			Subsystem: "booker",
			Name:      "message_duration_seconds",
			Help:      "Histogram observing the message processing duration",
			Buckets:   prometheus.ExponentialBuckets(0.001, 2, 5),
		},
	)
)

func main() {
	flag.Parse()
	db, err := sqlx.Open(*databaseDriver, *databaseURL)
	if err != nil {
		log.Fatal(err)
	}

	// Create tables if not exist
	if _, err := db.Exec(postgres.UserServiceSchema + postgres.TokenServiceSchema); err != nil {
		log.Fatal("Couldn't create tables:", err)
	}

	defer db.Close()
	// database services
	us, err := postgres.NewUserService(db)
	if err != nil {
		log.Fatal(err)
	}

	ts, err := postgres.NewTokenService(db)
	if err != nil {
		log.Fatal(err)
	}

	prometheus.MustRegister(requestCounter)
	prometheus.MustRegister(messageCounter)
	prometheus.MustRegister(messageDupCounter)
	prometheus.MustRegister(errorCounter)
	prometheus.MustRegister(messageDurations)
	http.Handle("/metrics", promhttp.Handler())
	errorCounter.WithLabelValues("fatal")
	errorCounter.WithLabelValues("non-fatal")

	cache, err := lru.NewARC(cacheSize)
	if err != nil {
		log.Fatal(err)
	}

	handler := &messageHandler{
		UserService:  us,
		TokenService: ts,
	}

	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		log.Fatal(http.ListenAndServe(":8080", nil))
		cancel()
	}()

	client, err := pubsub.NewClient(ctx, *projectID)
	if err != nil {
		log.Fatal(err)
	}
	sub := client.Subscription(*subName)
	ok, err := sub.Exists(ctx)
	if err != nil {
		log.Fatal(err)
	}
	if !ok {
		sub, err = client.CreateSubscription(
			ctx,
			*subName,
			pubsub.SubscriptionConfig{
				Topic:       client.Topic(*topicName),
				AckDeadline: 10 * time.Minute,
			},
		)
		if err != nil {
			log.Fatal(err)
		}
	}

	log.Println("Start receiving messages from queue")
	err = sub.Receive(ctx, func(ctx context.Context, m *pubsub.Message) {
		start := time.Now()
		messageCounter.Inc()
		_, found := cache.Get(m.ID)
		if found {
			log.Printf("Found duplicated message %s", m.ID)
			messageDupCounter.Inc()
			m.Ack()
			return
		}
		var s struct{}
		cache.Add(m.ID, s)
		if ok, err := handler.handle(m); err != nil {
			log.Printf("Message: %#v", m.Attributes)
			if !ok {
				m.Nack()
				errorCounter.WithLabelValues("fatal").Inc()
				log.Println("Fatal Error:", err)
				return
			}
			// Log non fatal errors but still ack message
			errorCounter.WithLabelValues("non-fatal").Inc()
			log.Println("Error:", err)
		}
		m.Ack()
		messageDurations.Observe(time.Since(start).Seconds())
	})
	if err != nil {
		log.Fatal(err)
	}
}

type messageHandler struct {
	UserService  latencyAt.UserService
	TokenService latencyAt.TokenService
}

// handle() handles messages and return true if the message was accounted for.
// It also return an validation error.
func (h *messageHandler) handle(m *pubsub.Message) (bool, error) {
	auth, ok := m.Attributes["authorization"]
	if !ok {
		return true, errors.ErrAuthorizationMissing
	}
	fields := strings.Fields(auth)
	if len(fields) != 2 {
		return true, errors.ErrAuthorizationMissing
	}
	if fields[0] != "Bearer" {
		return true, errors.ErrAuthorizationMissing
	}
	token, err := h.TokenService.TokenByToken(fields[1])
	if err != nil {
		// If the token wasn't found, we did something wrong otherwise
		// this message couldn't be here. So let's not block processing
		// messages.
		return err == errors.ErrTokenNotFound, err
	}

	// FIXME: We probably want to batch up the requests somehow
	requestCounter.Inc()
	return true, h.UserService.Balance(&latencyAt.User{ID: token.UserID}, -1)
}
